using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    
    public interface ICategoryService
    {
        IEnumerable<Categories> GetAll();

    }
    
    public class CategoryService: ICategoryService
    {
        private readonly AppSettings _appSettings;
        private DbContextDefault _context;

        public CategoryService(IOptions<AppSettings> appSettings, DbContextDefault dbContextDefault)
        {
            _appSettings = appSettings.Value;
            _context = dbContextDefault;
        }
        
        public IEnumerable<Categories> GetAll()
        {
            return _context.Categories.ToList();
        }
    }
}

