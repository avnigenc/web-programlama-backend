using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        Task<User> Register(User user);
        IEnumerable<User> GetAll();
        User GetUserById(int id);
        User UpdateUser(User user);
    }

    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private DbContextDefault _context;

        public UserService(IOptions<AppSettings> appSettings, DbContextDefault dbContextDefault)
        {
            _appSettings = appSettings.Value;
            _context = dbContextDefault;
        }

        public User Authenticate(string email, string password)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email == email && x.Password == password);
            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            return user;
        }

        public User UpdateUser(User user)
        {
            var foundUser = _context.Users.SingleOrDefault(x => x.Id == user.Id);
            foundUser.Email = user.Email;
            foundUser.FirstName = user.FirstName;
            foundUser.LastName = user.LastName;
            _context.Users.Update(foundUser);
            _context.SaveChanges();
            return foundUser;

        }
        public async Task<User> Register(User user)
        {
            try
            {
                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();
                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users.ToList();
        }

        public User GetUserById(int id)
        {
            return _context.Users.Include(i => i.UserType).SingleOrDefault(u => u.Id == id);
        }
    }
}