using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IEventLookUpService
    {
        Task<EventLookUp> CreateEvent(EventLookUp newEvent);
        IEnumerable<EventLookUp> GetAll();
        IEnumerable<EventLookUp> GetEventsById(int id);
    }

    public class EventLookUpService : IEventLookUpService
    {
        private readonly AppSettings _appSettings;
        private DbContextDefault _context;

        public EventLookUpService(IOptions<AppSettings> appSettings, DbContextDefault dbContextDefault)
        {
            _appSettings = appSettings.Value;
            _context = dbContextDefault;
        }

        public async Task<EventLookUp> CreateEvent(EventLookUp newEvent)
        {
            try
            {
                await _context.EventLookUp.AddAsync(newEvent);
                await _context.SaveChangesAsync();
                return newEvent;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<EventLookUp> GetAll()
        {
            return _context.EventLookUp.ToList();
        }

        public IEnumerable<EventLookUp> GetEventsById(int id)
        {
            return _context.EventLookUp
                .Include(i => i.Event)
                    .ThenInclude(ti => ti.City)
                .Include(i => i.Event)
                    .ThenInclude(ti => ti.Category)
                .Include(i => i.User).Where(u => u.UserId == id);
        }
    }
}