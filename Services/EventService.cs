using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IEventService
    {
        Task<Event> CreateEvent(Event newEvent);
        IEnumerable<Event> GetAll();
        IEnumerable<Event> GetEventsById(int id);
    }

    public class EventService : IEventService
    {
        private readonly AppSettings _appSettings;
        private DbContextDefault _context;

        public EventService(IOptions<AppSettings> appSettings, DbContextDefault dbContextDefault)
        {
            _appSettings = appSettings.Value;
            _context = dbContextDefault;
        }

        public async Task<Event> CreateEvent(Event newEvent)
        {
            try
            {
                await _context.Events.AddAsync(newEvent);
                await _context.SaveChangesAsync();
                return newEvent;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<Event> GetAll()
        {
            return _context.Events.Include(i => i.City).Include(i=>i.Category).ToList();
        }

        public IEnumerable<Event> GetEventsById(int id)
        {
            return _context.Events.Include(i => i.City).Include(i => i.Category).Where(u => u.UserId == id);
        }
    }
}