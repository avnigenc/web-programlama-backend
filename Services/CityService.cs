using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    
    public interface ICityService
    {
        IEnumerable<City> GetAll();

    }
    
    public class CityService: ICityService
    {
        private readonly AppSettings _appSettings;
        private DbContextDefault _context;

        public CityService(IOptions<AppSettings> appSettings, DbContextDefault dbContextDefault)
        {
            _appSettings = appSettings.Value;
            _context = dbContextDefault;
        }
        
        public IEnumerable<City> GetAll()
        {
            return _context.Cities.ToList();
        }
    }
}

