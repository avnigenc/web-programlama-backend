using Microsoft.EntityFrameworkCore;

namespace WebApi.Entities
{
    public class DbContextDefault:DbContext
    {
        public DbContextDefault(DbContextOptions<DbContextDefault> options):base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<EventLookUp> EventLookUp { get; set; }
    }
}