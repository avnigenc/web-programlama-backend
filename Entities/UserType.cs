using System.Collections.Generic;

namespace WebApi.Entities
{
    public class UserType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public ICollection<User> User { get; set; }

    }
}