using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WebApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public int UserTypeId { get; set; }

        public UserType UserType { get; set; }
        
        public ICollection<EventLookUp> EventLookUps { get; set; }

        
    }
}
