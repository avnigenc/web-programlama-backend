using System.Collections.Generic;

namespace WebApi.Entities
{
    public class Categories
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public ICollection<Event> Event { get; set; }
    }
}