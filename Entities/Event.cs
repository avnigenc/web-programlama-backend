using System;
using System.Collections.Generic;

namespace WebApi.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartingDate { get; set; }
        public float Price { get; set; }
        public string Location { get; set; }
        public int CategoryId { get; set; }
        public int CityId { get; set; }
        public int Capacity { get; set; }
        public City City { get; set; }
        public Categories Category { get; set; }

        public ICollection<EventLookUp> EventLookUps { get; set; }
    }
}