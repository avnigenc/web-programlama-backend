using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class GetUserByIdModel
    {
        [Required]
        public int Id { get; set; }
    }
}