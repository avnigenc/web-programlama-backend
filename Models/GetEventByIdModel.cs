using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class GetEventByIdModel
    {
        [Required]
        public int Id { get; set; }
    }
}