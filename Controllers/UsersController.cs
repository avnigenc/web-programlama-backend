﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Models;
using System.Linq;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Email, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }
        
        
        [AllowAnonymous]
        [HttpPost("updateUser")]
        public IActionResult UpdateUser([FromBody]User user)
        {
            var updatedUser = _userService.UpdateUser(user);
            return Ok(updatedUser);
        }
        
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]User user)
        {
            var newUser = _userService.Register(user);
            return Ok(newUser);
        }
        
        [AllowAnonymous]
        [HttpPost("getUserById")]
        public IActionResult Register([FromBody]GetUserByIdModel userByIdModel)
        {
            var user = _userService.GetUserById(userByIdModel.Id);
            return Ok(user);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
    }
}
