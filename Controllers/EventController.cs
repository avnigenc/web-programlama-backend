using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Entities;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventController: ControllerBase
    {
        private IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }
        
        
        [AllowAnonymous]
        [HttpPost("createEvent")]
        public IActionResult createEvent([FromBody] Event eventRequest)
        {
            var newEvent = _eventService.CreateEvent(eventRequest);
            return Ok(newEvent);
        }
        
        [AllowAnonymous]
        [HttpPost("getEventsById")]
        public IActionResult Register([FromBody]GetEventByIdModel eventByIdModel)
        {
            var events = _eventService.GetEventsById(eventByIdModel.Id);
            return Ok(events);
        }
        
        [HttpGet("getAllEvents")]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            var events = _eventService.GetAll();
            return Ok(events);
        }

    }
    
}


