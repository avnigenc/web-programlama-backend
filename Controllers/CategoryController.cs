using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController: ControllerBase
    {
        private ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        
        
        [AllowAnonymous]
        [HttpGet("getAllCategories")]
        public IActionResult getAllCategories()
        {
            var categories = _categoryService.GetAll();
            return Ok(categories);
        }

    }
}


