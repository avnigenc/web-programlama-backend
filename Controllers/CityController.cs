using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CityController: ControllerBase
    {
        private ICityService _cityService;

        public CityController(ICityService cityService)
        {
            _cityService = cityService;
        }
        
        
        [AllowAnonymous]
        [HttpGet("getAllCities")]
        public IActionResult GetAllCities()
        {
            var cities = _cityService.GetAll();
            return Ok(cities);
        }

    }
}


