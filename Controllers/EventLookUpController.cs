using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Entities;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventLookUpController: ControllerBase
    {
        private IEventLookUpService _eventLookUpService;

        public EventLookUpController(IEventLookUpService eventLookUpService)
        {
            _eventLookUpService = eventLookUpService;
        }
        
        
        [AllowAnonymous]
        [HttpPost("createEventLookUp")]
        public IActionResult CreateEvent([FromBody]EventLookUp eventLookUp)
        {
            var newEvent = _eventLookUpService.CreateEvent(eventLookUp);
            return Ok(newEvent);
        }
        
        [AllowAnonymous]
        [HttpPost("getEventsById")]
        public IActionResult GetEventsById([FromBody]GetEventByIdModel eventByIdModel)
        {
            var events = _eventLookUpService.GetEventsById(eventByIdModel.Id);
            return Ok(events);
        }

    }
    
}


