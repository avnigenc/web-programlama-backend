﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class modelchanged7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_EventLookUp_EventId",
                table: "EventLookUp",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_EventLookUp_UserId",
                table: "EventLookUp",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_EventLookUp_Events_EventId",
                table: "EventLookUp",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EventLookUp_Users_UserId",
                table: "EventLookUp",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EventLookUp_Events_EventId",
                table: "EventLookUp");

            migrationBuilder.DropForeignKey(
                name: "FK_EventLookUp_Users_UserId",
                table: "EventLookUp");

            migrationBuilder.DropIndex(
                name: "IX_EventLookUp_EventId",
                table: "EventLookUp");

            migrationBuilder.DropIndex(
                name: "IX_EventLookUp_UserId",
                table: "EventLookUp");
        }
    }
}
